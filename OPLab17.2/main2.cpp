#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

void sortAbs(int[], int);
void setArray(int[], int);
void printArray(int[], int);

int main() {
	srand(time(0));
	const int n = 10;
	int arr[n];

	setArray(arr, n);
	printArray(arr, n);
	sortAbs(arr, n);
	printArray(arr, n);

	return 0;
}

void sortAbs(int arr[], int SIZE) {
	int step = SIZE / 2;
	while (step > 0)
	{
		for (int i = 0; i < (SIZE - step); i++)
		{
			int j = i;
			while (j >= 0 && abs(arr[j]) > abs(arr[j + step]))
			{
				int c = arr[j];
				arr[j] = arr[j + step];
				arr[j + step] = c;
				j--;
			}
		}
		step = step / 2;
	}	
}

void setArray(int	arr[], int n) {
	for (int i = 0; i < n; i++) {
		arr[i] = -100 + rand() % 200;
	}
}
void printArray(int	arr[], int n) {
	for (int i = 0; i < n; i++) {
		printf("%3d | ", arr[i]);
	}
	printf("\n");
}
