#define _CRT_SECURE_NO_WARNINGS
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <windows.h>

double Leng(double xA, double yA, double xB, double yB) {
	return sqrt(pow(xA - xB, 2) + pow(yA - yB, 2));
}

double Perim(double xA, double yA, double xB, double yB, double xC, double yC) {
	return Leng(xA, yA, xB, yB) + Leng(xA, yA, xC, yC) + Leng(xC, yC, xB, yB);
}

double Area(double xA, double yA, double xB, double yB, double xC, double yC) {
	double p = Perim(xA, yA, xB, yB, xC, yC) / 2;
	return sqrt(p * (p - Leng(xA, yA, xB, yB)) * (p - Leng(xA, yA, xC, yC)) * (p - Leng(xB, yB, xC, yC)));
}

double Dist(double xP, double yP, double xA, double yA, double xB, double yB) {
	return 2 * Area(xP, yP, xA, yA, xB, yB) / Leng(xA, yA, xB, yB);
}

void Altitudes(double xA, double yA, double xB, double yB, double xC, double yC, double &hA, double &hB, double &hC) {
	hA = Dist(xA, yA, xB, yB, xC, yC);
	hB = Dist(xB, yB, xA, yA, xC, yC);
	hC = Dist(xC, yC, xA, yA, xB, yB);
}

void setCoors(double xy[4][2], char point) {
	printf("������ ����� ����� ���������� ����� %c ", point);
	point -= 65;
	scanf("%lf %lf", &xy[point][0], &xy[point][1]);
}

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	double xy[4][2];

	setCoors(xy, 'A');
	setCoors(xy, 'B');
	setCoors(xy, 'C');
	setCoors(xy, 'D');
	
	printf("\n|AB| = %lf\n", Leng(xy[0][0], xy[0][1], xy[1][0], xy[1][1]));
	printf("|AC| = %lf\n", Leng(xy[0][0], xy[0][1], xy[2][0], xy[2][1]));	
	printf("|AD| = %lf\n", Leng(xy[0][0], xy[0][1], xy[3][0], xy[3][1]));

	printf("P(ABC) = %lf\n", Perim(xy[0][0], xy[0][1], xy[1][0], xy[1][1], xy[2][0], xy[2][1]));
	printf("P(ABD) = %lf\n", Perim(xy[0][0], xy[0][1], xy[1][0], xy[1][1], xy[3][0], xy[3][1]));
	printf("P(ACD) = %lf\n", Perim(xy[0][0], xy[0][1], xy[2][0], xy[2][1], xy[3][0], xy[3][1]));

	printf("S(ABC) = %lf\n", Area(xy[0][0], xy[0][1], xy[1][0], xy[1][1], xy[2][0], xy[2][1]));
	printf("S(ABD) = %lf\n", Area(xy[0][0], xy[0][1], xy[1][0], xy[1][1], xy[3][0], xy[3][1]));
	printf("S(ACD) = %lf\n", Area(xy[0][0], xy[0][1], xy[2][0], xy[2][1], xy[3][0], xy[3][1]));

	double xP, yP;
	printf("\n������ ����� ����� ���������� ����� P ");
	scanf("%lf %lf", &xP, &yP);
	printf("\nD(P, AB) = %lf\n", Dist(xP, yP, xy[0][0], xy[0][1], xy[1][0], xy[1][1]));
	printf("D(P, AC) = %lf\n", Dist(xP, yP, xy[0][0], xy[0][1], xy[2][0], xy[2][1]));
	printf("D(P, BC) = %lf\n", Dist(xP, yP, xy[1][0], xy[1][1], xy[2][0], xy[2][1]));

	double hA, hB, hC, hD;

	Altitudes(xy[0][0], xy[0][1], xy[1][0], xy[1][1], xy[2][0], xy[2][1], hA, hB, hC);
	printf("\nABC:\nhA = %lf\n", hA);
	printf("hB = %lf\n", hB);
	printf("hC = %lf\n", hC);

	Altitudes(xy[0][0], xy[0][1], xy[1][0], xy[1][1], xy[3][0], xy[3][1], hA, hB, hD);
	printf("\nABD:\nhA = %lf\n", hA);
	printf("hB = %lf\n", hB);
	printf("hD = %lf\n", hD);

	Altitudes(xy[0][0], xy[0][1], xy[2][0], xy[2][1], xy[3][0], xy[3][1], hA, hC, hD);
	printf("\nACD:\nhA = %lf\n", hA);
	printf("hC = %lf\n", hC);
	printf("hD = %lf\n", hD);

	return 0;
}